<?php

namespace Ls\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Page controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Page entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsPageBundle:Page', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        return $this->render('LsPageBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }

}
