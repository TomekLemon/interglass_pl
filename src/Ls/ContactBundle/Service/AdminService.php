<?php

namespace Ls\ContactBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $parent, $route, $set) {
        $item = $parent->addChild('Wiadomości z formularza kontaktowego', array(
            'route' => 'ls_admin_contact',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_contact':
                case 'ls_admin_contact_show':
                case 'ls_admin_contact_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Wiadomości z formularza kontaktowego');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_contact')));
    }
}

