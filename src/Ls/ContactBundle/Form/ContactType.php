<?php

namespace Ls\ContactBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->container = $serviceContainer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, array(
                'label'       => $this->getNameLabel(),
                'required'    => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => $this->getNotBlankMessage()
                    )),
                ),
            )
        );
        $builder->add('email', null, array(
                'label'       => $this->getEmailLabel(),
                'required'    => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => $this->getNotBlankMessage()
                    )),
                    new Email(array(
                        'message' => $this->getEmailMessage(),
                    )),
                ),
            )
        );
        $builder->add('phone', null, array(
                'label'       => $this->getPhoneLabel(),
                'required'    => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => $this->getNotBlankMessage()
                    )),
                ),
            )
        );
        $builder->add('content', TextareaType::class, array(
                'label'       => $this->getContentLabel(),
                'required'    => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => $this->getNotBlankMessage()
                    )),
                ),
            )
        );
//        $builder->add('google_recaptcha', GoogleRecaptchaType::class, array(
//                'label' => 'Ochrona antyspamowa',
//                'error_bubbling' => false,
//                'mapped' => false,
//                'constraints' => array(
//                    new GoogleRecaptcha()
//                )
//            )
//        );
        $builder->add('submit', SubmitType::class, array(
                'label' => $this->getSubmitLabel(),
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'form_contact';
    }

    private function getNameLabel()
    {
        $cms_config = $this->container->get('cms_config');
        $label = $cms_config->get('contact_form_name', 'Imię i nazwisko');

        return $label;
    }

    private function getEmailLabel()
    {
        $cms_config = $this->container->get('cms_config');
        $label = $cms_config->get('contact_form_email', 'Adres e-mail');

        return $label;
    }

    private function getPhoneLabel()
    {
        $cms_config = $this->container->get('cms_config');
        $label = $cms_config->get('contact_form_phone', 'Telefon');

        return $label;
    }

    private function getContentLabel()
    {
        $cms_config = $this->container->get('cms_config');
        $label = $cms_config->get('contact_form_content', 'Treść wiadomości');

        return $label;
    }

    private function getSubmitLabel()
    {
        $cms_config = $this->container->get('cms_config');
        $label = $cms_config->get('contact_form_submit', 'Wyślij');

        return $label;
    }

    private function getNotBlankMessage()
    {
        $cms_config = $this->container->get('cms_config');
        $message = $cms_config->get('contact_form_message_not_blank', 'Wypełnij pole');

        return $message;
    }

    private function getEmailMessage()
    {
        $cms_config = $this->container->get('cms_config');
        $message = $cms_config->get('contact_form_message_email', 'Podaj poprawny adres e-mail');

        return $message;
    }
}
