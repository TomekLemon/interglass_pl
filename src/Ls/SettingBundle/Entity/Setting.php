<?php

namespace Ls\SettingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Setting
 *
 * @ORM\Entity
 * @ORM\Table(name="setting")
 */
class Setting
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=191, unique=true)
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    protected $file;

    protected $bigWidth = 320;
    protected $bigHeight = 360;
    protected $mediumWidth = 320;
    protected $mediumHeight = 290;
    protected $smallWidth = 160;
    protected $smallHeight = 180;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Setting
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Setting
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Setting
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Setting
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Setting
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        if (is_null($this->getDescription())) {
            return 'NULL';
        }

        return $this->getDescription();
    }

    public function getThumbSize($type)
    {
        $size = array();
        switch ($type) {
            case 'big':
                $size['width'] = $this->bigWidth;
                $size['height'] = $this->bigHeight;
                break;
            case 'medium':
                $size['width'] = $this->mediumWidth;
                $size['height'] = $this->mediumHeight;
                break;
            case 'small':
                $size['width'] = $this->smallWidth;
                $size['height'] = $this->smallHeight;
                break;
        }

        return $size;
    }

    public function getThumbWebPath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'big':
                    $sThumbName = Tools::thumbName($this->photo, '_b');
                    break;
                case 'medium':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
                case 'small':
                    $sThumbName = Tools::thumbName($this->photo, '_s');
                    break;
            }

            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'big':
                    $sThumbName = Tools::thumbName($this->photo, '_b');
                    break;
                case 'medium':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
                case 'small':
                    $sThumbName = Tools::thumbName($this->photo, '_s');
                    break;
            }

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize()
    {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width'  => $temp[0],
            'height' => $temp[1],
        );

        return $size;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function deletePhoto()
    {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_b = Tools::thumbName($filename, '_b');
            $filename_m = Tools::thumbName($filename, '_m');
            $filename_s = Tools::thumbName($filename, '_s');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_b)) {
                @unlink($filename_b);
            }
            if (file_exists($filename_m)) {
                @unlink($filename_m);
            }
            if (file_exists($filename_s)) {
                @unlink($filename_s);
            }
        }
    }

    public function getPhotoAbsolutePath()
    {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath()
    {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/settings';
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs()
    {
        if (null !== $this->getPhotoAbsolutePath()) {
            $sFileName = $this->getPhoto();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameB = Tools::thumbName($sSourceName, '_b');
            $aThumbSizeB = $this->getThumbSize('big');
            $thumb->adaptiveResize($aThumbSizeB['width'] + 2, $aThumbSizeB['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeB['width'], $aThumbSizeB['height']);
            $thumb->save($sThumbNameB);

            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameM = Tools::thumbName($sSourceName, '_m');
            $aThumbSizeM = $this->getThumbSize('medium');
            $thumb->adaptiveResize($aThumbSizeM['width'] + 2, $aThumbSizeM['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeM['width'], $aThumbSizeM['height']);
            $thumb->save($sThumbNameM);

            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameS = Tools::thumbName($sSourceName, '_s');
            $aThumbSizeS = $this->getThumbSize('small');
            $thumb->adaptiveResize($aThumbSizeS['width'] + 2, $aThumbSizeS['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeS['width'], $aThumbSizeS['height']);
            $thumb->save($sThumbNameS);
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
}