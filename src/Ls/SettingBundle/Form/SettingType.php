<?php

namespace Ls\SettingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class SettingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', ChoiceType::class, array(
            'choices'           => $this->getTypeChoices(),
            'choices_as_values' => true,
            'label'             => 'Rodzaj ustawienia',
        ));
        $builder->add('label', null, array(
            'label'       => 'Etykieta',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole',
                )),
            ),
        ));
        $builder->add('description', null, array(
            'label' => 'Opis',
        ));
        $builder->add('value', null, array(
            'label' => 'Wartość',
            'attr'  => array(
                'rows' => 5,
            ),
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            $size = $object->getThumbSize('big');

            $form->add('file', FileType::class, array(
                'label'       => 'Nowe zdjęcie',
                'constraints' => array(
                    new Image(array(
                        'minWidth'         => $size['width'],
                        'minHeight'        => $size['height'],
                        'minWidthMessage'  => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                        'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                    )),
                ),
            ));
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\SettingBundle\Entity\Setting',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'form_admin_setting';
    }

    public function getTypeChoices()
    {
        $types = array(
            'Tekst'   => 'text',
            'Zdjęcie' => 'photo',
        );

        return $types;
    }
}
