<?php

namespace Ls\CoreBundle\Controller;

use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    protected $containerBuilder;

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $slider_photos = $em->createQueryBuilder()
            ->select('e')
            ->from('LsSliderBundle:SliderPhoto', 'e')
	        ->orderBy('e.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        $menu_items = $em->createQueryBuilder()
            ->select('m')
            ->from('LsMenuBundle:MenuItem', 'm')
            ->where('m.location = :location')
            ->orderBy('m.arrangement', 'asc')
            ->setParameter('location', 'menu_top')
            ->getQuery()
            ->getResult();

        $page = null;

        foreach ($menu_items as $item) {
            if ($item->getType() == 'section' && $item->getSection() == 'about') {
                $slug = 'default';
                foreach ($item->getRouteParametersArray() as $k => $param) {
                    if ($k == 'slug') {
                        $slug = $param;
                    }
                }
                $page = $em->createQueryBuilder()
                    ->select('p')
                    ->from('LsPageBundle:Page', 'p')
                    ->where('p.slug = :slug')
                    ->setParameter('slug', $slug)
                    ->getQuery()
                    ->getOneOrNullResult();
            }
        }

        $offer = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOfferBundle:Offer', 'e')
            ->orderBy('e.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        $offer_quote = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('offer_text')->getValue();
        $offer_photo_1 = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('offer_photo_1');
        $offer_photo_2 = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('offer_photo_2');

        $manufacturing = $em->createQueryBuilder()
            ->select('e')
            ->from('LsManufacturingBundle:Manufacturing', 'e')
            ->orderBy('e.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        $manufacturing_quote = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('manufacturing_text')->getValue();
        $manufacturing_photo = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('manufacturing_photo');

        return $this->render('LsCoreBundle:Default:index.html.twig', array(
            'slider_photos'       => $slider_photos,
            'offer_photo_2'       => $offer_photo_2,
            'offer_photo_1'       => $offer_photo_1,
            'offer_quote'         => $offer_quote,
            'offer'               => $offer,
            'manufacturing_photo' => $manufacturing_photo,
            'manufacturing_quote' => $manufacturing_quote,
            'manufacturing'       => $manufacturing,
            'page'                => $page,
            'menu_items'          => $menu_items,
        ));
    }

    public function adminAction()
    {
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach ($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach ($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard,
        ));
    }

    public function KCFinderAction(Request $request)
    {
        $upload_dir = $this->get('kernel')->getRootDir() . '/../web/upload/pliki';

        $_SESSION['KCFINDER']['disabled'] = false;
        $_SESSION['KCFINDER']['uploadDir'] = $upload_dir;

        $getParameters = $request->query->all();

        
        return new RedirectResponse(
            $request->getBasePath() . 
            '/bundles/lscore/common/kcfinder-3.20/browse.php?' . 
            http_build_query($getParameters));
    }
}
