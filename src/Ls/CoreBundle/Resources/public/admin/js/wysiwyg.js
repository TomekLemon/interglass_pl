$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/front/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Nagłówek', element: 'p', attributes: {'class': 'header'}},
            {name: 'Podpis', element: 'p', attributes: {'class': 'signature'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
});

CKEDITOR.on(
    'instanceReady',
    function (ev) {
        var $script = document.createElement('script'),
            $editor_instance = CKEDITOR.instances[ev.editor.name];

        $script.src = '//use.typekit.net/mea1ccg.js';
        $script.onload = function () {
            try {
                $editor_instance.window.$.Typekit.load();
            } catch (e) {
            }
        };

        $editor_instance.document.getHead().$.appendChild($script);
    }
);