$(document).on('click', '.show-menu', function () {
    if (!$('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeIn(250);
    }
});

$(document).on('click', '.hide-menu', function () {
    if ($('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeOut(250);
    }
});

$('.nav').on('click', 'a', function (event) {
    event.preventDefault();

    animateMenu($(this).attr('href'))
});

$('#main-slider').on('click', 'a', function (event) {
    event.preventDefault();

    animateMenu($(this).attr('href'))
});

$('.to-top').on('click', function () {
    var body = $("html, body");

    body.stop(true, true).animate({scrollTop: 0}, 1000, 'swing');
});

$(document).ready(function () {
    var slider = $('#slider');

    if (slider.length > 0) {
        slider.cycle({
            fx: 'scrollHorz',
            slides: '.slide',
            timeout: 8000,
            speed: 1000,
            prev: '#slider-prev',
            next: '#slider-next'
        });
    }
});

$(document).on("scroll", onScroll);

function onScroll() {
    var headerHeight = $('header').outerHeight();
    var scrollPos = $(document).scrollTop();

    if (scrollPos >= headerHeight) {
        $('.floating-header').addClass('visible');
    } else {
        $('.floating-header').removeClass('visible');
    }

    $('.navbar-nav a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));

        console.log(refElement.attr('id'));
        console.log(scrollPos);
        console.log(refElement.position().top);
        console.log(refElement.position().top + refElement.height());

        if (refElement.length > 0 && refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            currLink.parent().addClass('active');
        } else {
            currLink.parent().removeClass('active');
        }
    });
}

function sendContact() {
    var form = $('#form_contact');
    var data = form.serialize();
    var url = form.attr('action');

    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function (response) {
            $('.contact-form-container').html(response);
        }
    });

    return false;
}

function animateMenu(id) {
    var body = $("html, body");
    var top = $(id).position().top;

    body.stop(true, true).animate({scrollTop: top}, 1000, 'swing');
    $('.menu-mobile').fadeOut(250);
}

$('.indicator').on('click', function() {
    if(parseInt($('#program').css('right')) != 0) {
        $('#program').stop(true, false).animate({
            right: 0
        }, 500);
    } else {
        $('#program').stop(true, false).animate({
            right: -360
        }, 500);
    }
});

$('.indicator2').on('click', function() {
    if(parseInt($('#program2').css('right')) != 0) {
        $('#program2').stop(true, false).animate({
            right: 0
        }, 500);
    } else {
        $('#program2').stop(true, false).animate({
            right: -360
        }, 500);
    }
});

$('.mobile-program').on('click', function (event) {
    $('.menu-mobile').fadeOut(250);
});