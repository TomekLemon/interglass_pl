-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: interglass
-- ------------------------------------------------------
-- Server version	5.6.27-75.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'Halo','sto_cercando@onet.pl','Halo','Halo','2016-09-15 20:30:56'),(2,'Halo','sto_cercando@onet.pl','Halo','Halo','2016-09-15 20:30:59'),(3,'Arkadiusz Rozwadowski','rozwadowskiarkadiusz@gmail.com','501705555','Od kiedy planujecie Państwo hartować szkło i czy będzie możliwość hartowania szkła o długości 2,9m.\r\n\r\nPozdrawiam\r\nArkadiusz Rozwadowski','2016-12-05 15:53:19'),(4,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:28:57'),(5,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:29:00'),(6,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:29:03'),(7,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:29:05'),(8,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:29:10'),(9,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:29:13'),(10,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:29:14'),(11,'Barbara Balog','b.balog@tlen.pl','600320397','nnnnnn','2016-12-29 15:29:16'),(12,'Arkadiusz Rozwadowski','rozwadowskiarkadiusz@gmail.com','501705555','Witam,\r\nchciałbym zapytać czy mógłbym u Państwa wykonać fazę 25mm  na własnym lustrze o wymiarze 3052 x 972, lub czy mógłbym u Państwa zamówić takie lustro (zależałoby mi żeby lustro było z firmy AGC lub Saint-Gobain).\r\n\r\nPozdrawiam\r\nArkadiusz Rozwadowski','2017-01-16 12:43:20'),(13,'jimos45812rt1@hotmail.com','jimos45812rt1@hotmail.com','jimos45812rt1@hotmail.com','zLBYvr http://www.LnAJ7K8QSpkiStk3sLL0hQP6MO2wQ8gO.com','2017-05-20 00:53:35'),(14,'VirgilGer','serverftp2017@mail.ru','83275641622','https://0daymusic.org/','2017-07-06 18:17:12'),(15,'ecrev22vtv@hotmail.com','ecrev22vtv@hotmail.com','ecrev22vtv@hotmail.com','usgtcA http://www.LnAJ7K8QSpkiStk3sLL0hQP6MO2wQ8gO.com','2017-07-08 16:26:25'),(16,'Mazur Dawid','mazurdawid12@gmail.com','881563607','Witam, ja pisze z zapytaniem czy obecnie macie przyjęcia do pracy ?','2017-07-25 19:00:30');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturing`
--

DROP TABLE IF EXISTS `manufacturing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `arrangement` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturing`
--

LOCK TABLES `manufacturing` WRITE;
/*!40000 ALTER TABLE `manufacturing` DISABLE KEYS */;
INSERT INTO `manufacturing` VALUES (1,'Cięcie','Posiadamy numerycznie sterowany stół do rozkroju firmy Bottero. Daje nam to możliwość dostosowania się do potrzeb klienta zarówno jeśli chodzi o grubość (od 3-19mm) materiału jak i pożądany kształt. Przecinamy tafle o maksymalnym wymiarze 3210x2550mm.',1,'2016-08-24 14:02:07',NULL),(2,'Fazowanie','Posiadamy fazowarkę pionową firmy Bavelloni. Oferujemy fazowanie na odcinkach prostych. Wykonujemy fazę od 2 do 30mm.',2,'2016-08-24 14:02:22',NULL),(3,'Szlifowanie','Posiadamy linię do szlifowania firmy Szkło-Tech. Szlifujemy szkło i lustra o grubości od 3-19mm i dowolnym kształcie. W zależności od potrzeb klienta możemy nadać krawędziom formę C-kant lub trapez.',3,'2016-08-24 14:03:08',NULL),(4,'Polerowanie','Na życzenie klienta na formatkach o dowolnym kształcie wykonujemy polerowanie krawędzi.',4,'2016-08-24 14:03:25',NULL),(5,'Wiercenie otworów','Jesteśmy w stanie wykonać otwory o średnicy 5-50mm na szkle o grubości od 3-19mm.',5,'2016-08-24 14:03:43',NULL),(6,'Podklejanie folią bezpieczną','Posiadamy maszynę do podklejania luster folią bezpieczną',6,'2016-08-24 14:04:03',NULL),(7,'Pakowanie formatek szklanych i lustrzanych w profile styropianowe','Na życzenie klienta pakujemy formatki szklane i lustrzane w estetyczne profile styropianowe i dodatkowo zabezpieczamy folią termokurczliwą.',7,'2016-08-24 14:04:22',NULL);
/*!40000 ALTER TABLE `manufacturing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D754D550727ACA70` (`parent_id`),
  CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_item`
--

LOCK TABLES `menu_item` WRITE;
/*!40000 ALTER TABLE `menu_item` DISABLE KEYS */;
INSERT INTO `menu_item` VALUES (1,NULL,'menu_top','section','O firmie','ls_page_show','{\"slug\":\"o-firmie\"}','#o-firmie','about','o-firmie',NULL,0,1),(2,NULL,'menu_top','section','Oferta','ls_page_show',NULL,'#oferta','offer','oferta',NULL,0,2),(3,NULL,'menu_top','section','Obróbka','ls_page_show',NULL,'#obrobka','manufacturing','obrobka',NULL,0,3),(4,NULL,'menu_top','section','Kontakt','ls_page_show',NULL,'#kontakt','contact','kontakt',NULL,0,4);
/*!40000 ALTER TABLE `menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20160816102745'),('20160816122615'),('20160817092641'),('20160823042354'),('20160823095901'),('20160823100231'),('20160823140226'),('20160824055059'),('20160824112113'),('20160831083118'),('20160831091215');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offer`
--

DROP TABLE IF EXISTS `offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offer`
--

LOCK TABLES `offer` WRITE;
/*!40000 ALTER TABLE `offer` DISABLE KEYS */;
INSERT INTO `offer` VALUES (1,'Lustro srebrne 3mm - 4mm','offer-image-57bd54053e2a6.png',1,'2016-08-24 10:00:03',NULL),(2,'Szkło float 3mm, 4mm, 5mm, 6mm, 8mm','offer-image-57bd5420b2225.png',2,'2016-08-24 10:00:32',NULL),(3,'Szkło barwione w masie','offer-image-57bd54322b92f.png',3,'2016-08-24 10:00:50',NULL),(4,'Szkło lakierowane (lacobele)','offer-image-57bd54436fa8f.png',4,'2016-08-24 10:01:07',NULL),(5,'Ornament','offer-image-57bd5453dbbf0.png',5,'2016-08-24 10:01:23',NULL),(6,'Decormat','offer-image-57bd54639784f.png',6,'2016-08-24 10:01:39',NULL),(7,'Witraże','offer-image-57bd547138d70.png',7,'2016-08-24 10:01:53',NULL),(8,'Sitodruk','offer-image-57c6cefe4f303.png',8,'2016-08-31 14:35:10',NULL);
/*!40000 ALTER TABLE `offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'O firmie','o-firmie','o-firmie',0,'Nasza Firma jest jednym z liderów w branży szklarskiej w południowo-wschodniej części Polski','<p class=\"header\">\r\n	Firma &bdquo;Inter-Glass&ldquo; powstała w 1999r. Zajmujemy się obr&oacute;bką szkła płaskiego i luster do fabryk mebli.</p>\r\n<p>\r\n	Naszymi odbiorcami są fabryki mebli w Polsce, Białorusi, Ukrainie i Chorwacji. Wysoka jakość naszych produkt&oacute;w pozwoliła Firmie zaistnieć i ugruntować silną pozycję na rynkach Europy zachodniej. Połowa naszej produkcji trafia do fabryk mebli w Niemczech.</p>\r\n<p>\r\n	Nasza Firma jest jednym z lider&oacute;w w branży szklarskiej w południowo-wschodniej części Polski. Zawdzięczamy to wysoko wykwalifikowanym specjalistom i wysokiej jakości produktom. Pracujemy na maszynach renomowanych firm m.in. Bavelloni i Bottero.</p>\r\n<p>\r\n	Jesteśmy w stanie dopasować nasze produkty do potrzeb i oczekiwań naszych klient&oacute;w. Zapraszamy do zapoznania się z naszą szczeg&oacute;łową ofertą i do wsp&oacute;łpracy.</p>\r\n<p class=\"signature\">\r\n	Barbara i Włodzimierz Balog</p>','page-image-57c7e303136de.jpeg',0,'O firmie','firmie, Firma, „Inter-Glass“, powstała, 1999r, Zajmujemy, się, obróbką, szkła, płaskiego','Firma „Inter-Glass“ powstała w 1999r. Zajmujemy się obróbką szkła płaskiego i luster do fabryk mebli. Naszymi odbiorcami są fabryki mebli w Polsce,...','2016-08-16 10:50:31','2016-09-06 23:37:10');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'text','seo_description','Domyślna wartość meta tagu \"description\"','Firma „Inter-Glass“ powstała w 1999r. Zajmujemy się obróbką szkła płaskiego i luster do fabryk mebli. Naszymi odbiorcami są fabryki mebli w Polsce, Białorusi, Ukrainie i Chorwacji.',NULL),(2,'text','seo_keywords','Domyślna wartość meta tagu \"keywords\"',NULL,NULL),(3,'text','seo_title','Domyślna wartość meta tagu \"title\"','Inter-glass / obróbka szkła i luster',NULL),(4,'text','email_to_contact','Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego','biuro@inter-glass.net',NULL),(5,'text','slider_header','Slider - Nagłówek','Lider w branży szklarskiej',NULL),(6,'text','slider_text','Slider - Tekst','Wysoka jakość naszych produktów pozwoliła Firmie zaistnieć i ugruntować silną pozycję na rynkach Europy zachodniej',NULL),(7,'text','contact_firm','Kontakt - Nazwa firmy','P.P.H. „Inter-Glass“',NULL),(8,'text','contact_address','Kontakt - Adres','ul. Kolejowa 20, 23-200 Kraśnik',NULL),(9,'text','contact_phone','Kontakt - Numer telefonu','tel. +48 81 825 18 83\r\nkom. +48 600 320 397\r\nkom. +48 601 174 464',NULL),(10,'text','contact_email','Kontakt - Adresy e-mail','hartownia@inter-glass.net\r\nprodukcja@inter-glass.net',NULL),(11,'text','contact_nip','Kontakt - NIP','715 120 84 52',NULL),(12,'text','contact_regon','Kontakt - REGON','431152609',NULL),(13,'text','offer_text','Oferta - Hasło','Jesteśmy w stanie dopasować nasze produkty do potrzeb i oczekiwań naszych klientów',NULL),(14,'photo','offer_photo_1','Oferta - Zdjęcie główne',NULL,'setting-image-57d82a9bb17b8.jpeg'),(15,'photo','offer_photo_2','Oferta - Zdjęcie dodatkow (widoczne w wiekszych rozdzielczościach)',NULL,'setting-image-57c6bb7a762a9.jpeg'),(16,'text','manufacturing_text','Obróbka - Hasło','Pracujemy na maszynach renomowanych firm m.in. Bavelloni i Bottero',NULL),(17,'photo','manufacturing_photo','Obróbka - Zdjęcie',NULL,'setting-image-57c7e3c9e17c4.jpeg'),(18,'text','google_analytics','Kod śledzący Google Analytics','<script>\r\n(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\r\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n})(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');\r\n\r\nga(\'create\', \'UA-31095387-8\', \'auto\');\r\nga(\'send\', \'pageview\');\r\n\r\n</script>',NULL),(19,'text','slider_button','Slider - Tekst na przycisku','Poznaj ofertę',NULL),(20,'text','footer_copyright','Stopka strony - Copyright','© 2016 Interglass. Wszystkie prawa zastrzeżone.',NULL),(21,'text','footer_project','Stopka strony - Projekt i realizacja','Projekt i realizacja:',NULL),(22,'text','footer_up','Stopka strony - Tekst na przycisku \"Do góry\"','Do góry',NULL),(23,'text','contact_form_name','Formularz kontaktowy - Etykieta - Imię i nazwisko','Imię i nazwisko',NULL),(24,'text','contact_form_email','Formularz kontaktowy - Etykieta - Adres e-mail','Adres e-mail',NULL),(25,'text','contact_form_phone','Formularz kontaktowy - Etykieta - Telefon','Telefon',NULL),(26,'text','contact_form_content','Formularz kontaktowy - Etykieta - Treść wiadomości','Treść wiadomości',NULL),(27,'text','contact_form_submit','Formularz kontaktowy - Etykieta - Wyślij','Wyślij',NULL),(28,'text','contact_form_message_not_blank','Formularz kontaktowy - Komunikat - Wypełnij pole','Wypełnij pole',NULL),(29,'text','contact_form_message_email','Formularz kontaktowy - Komunikat - Podaj poprawny adres e-mail','Podaj poprawny adres e-mail',NULL);
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_photo`
--

DROP TABLE IF EXISTS `slider_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_photo`
--

LOCK TABLES `slider_photo` WRITE;
/*!40000 ALTER TABLE `slider_photo` DISABLE KEYS */;
INSERT INTO `slider_photo` VALUES (4,'slider-image-57c6c34b8faa4.jpeg',2),(6,'slider-image-57c7e14473aa7.jpg',7),(7,'slider-image-57c7e145547c5.jpg',6),(8,'slider-image-57c7e146261ea.jpg',9),(9,'slider-image-57da7ec4a843c.jpeg',5),(10,'slider-image-57cf36580b111.JPG',1),(11,'slider-image-57d08c22198f7.JPG',8),(15,'slider-image-57da801baad8f.jpeg',3),(16,'slider-image-57dd3a808d2f4.jpg',4);
/*!40000 ALTER TABLE `slider_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'wiktoria','wbalogw@gmail.com','d0961c9ede87320445e2ad6a7ec56c83','7MEuRFT+YK50Pi4fYiyIM6+xNJh5phth+iAmboLjez4fbftaKHSzbzrcO9whsTrz16JEfO5w4oBAWrwIzAhXrQ==',1,NULL,'[\"ROLE_ADMIN\"]','2016-09-06 18:22:30',NULL),(3,'admin','biuro@lemonadestudio.pl','e40e0bc6f3ebacbed6e0e19b772845a8','Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==',1,NULL,'[\"ROLE_ADMIN\",\"ROLE_ALLOWED_TO_SWITCH\",\"ROLE_USER\"]','2015-07-31 13:46:34',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-11 14:45:19
